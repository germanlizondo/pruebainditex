package com.inditex.prueba.repositories;

import com.inditex.prueba.exceptions.models.PriceNotFoundException;
import com.inditex.prueba.models.Price;
import com.inditex.prueba.repositories.abstractions.PriceJPARepository;
import com.inditex.prueba.repositories.impl.PriceRepositoryJPAImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class PriceRepositoryJPAImplTest {

    @Mock
    private PriceJPARepository priceJPARepository;

    @InjectMocks
    private PriceRepositoryJPAImpl priceRepository;

    @Test
    public void testGetPriceByDate() throws PriceNotFoundException {

        Timestamp dateToApplied = new Timestamp(System.currentTimeMillis());
        Long productId = 1L;
        Long brandId = 1L;
        Price expectedPrice = new Price();

        Mockito.when(priceJPARepository.getPriceByDate(dateToApplied, productId, brandId)).thenReturn(Optional.of(expectedPrice));

        Price price = priceRepository.getPriceByDate(dateToApplied, productId, brandId);

        assertNotNull(price);
    }

    @Test
    public void testGetPriceByDateNotFound() {

        Timestamp nonExistentDate = Timestamp.valueOf("2023-03-05 00:00:00");
        Long productId = 1L;
        Long brandId = 1L;

        Mockito.when(priceJPARepository.getPriceByDate(nonExistentDate, productId, brandId)).thenReturn(Optional.empty());

        assertThrows(PriceNotFoundException.class, () -> {
            priceRepository.getPriceByDate(nonExistentDate, productId, brandId);
        });
    }
}