package com.inditex.prueba.services;

import com.inditex.prueba.dto.PriceResponseDto;
import com.inditex.prueba.exceptions.models.PriceNotFoundException;
import com.inditex.prueba.mappers.PriceToDtoMapper;
import com.inditex.prueba.models.Price;
import com.inditex.prueba.repositories.PriceRepository;
import com.inditex.prueba.services.impl.PriceServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class PriceServiceImplTest {

    @InjectMocks
    private PriceServiceImpl priceService;

    @Mock
    private PriceRepository priceRepository;

    @Mock
    private PriceToDtoMapper priceToDtoMapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetPrice() throws PriceNotFoundException {

        Date dateApplied = new Date();
        Long productId = 1L;
        Long brandId = 2L;
        Price price = new Price();
        PriceResponseDto responseDto = new PriceResponseDto();

        when(priceRepository.getPriceByDate(new Timestamp(dateApplied.getTime()), productId, brandId)).thenReturn(price);
        when(priceToDtoMapper.priceModelToDtoResponse(price)).thenReturn(responseDto);


        PriceResponseDto result = priceService.getPrice(dateApplied, productId, brandId);


        assertEquals(responseDto, result);
    }
}
