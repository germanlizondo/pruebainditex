package com.inditex.prueba.exceptions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inditex.prueba.dto.PriceResponseDto;
import com.inditex.prueba.exceptions.dto.ErrorDtoResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
public class PriceNotFoundExceptionHandleTest {

    public static final String URI = "/price";

    public static final String DATE_PARAM = "dateApplied";
    public static final String PRODUCT_PARAM = "productId";
    public static final String BRAND_PARAM = "brandId";

    @Autowired
    protected WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    void testPriceNotFoundException() throws Exception {

        ErrorDtoResponse expected =
                new ErrorDtoResponse(HttpStatus.BAD_REQUEST, "Price not found", "/price");

        makeTestPetition("2020/06/14 10:00:00", "35455", "55", expected);
    }
    private void makeTestPetition(
            final String dateParam,
            final String productParam,
            final String brandParam,
            final ErrorDtoResponse expected)
            throws Exception {

        MultiValueMap<String, String> queryparams = new LinkedMultiValueMap<>();
        queryparams.add(DATE_PARAM, dateParam);
        queryparams.add(PRODUCT_PARAM, productParam);
        queryparams.add(BRAND_PARAM, brandParam);

        mockMvc
                .perform(MockMvcRequestBuilders.get(URI).queryParams(queryparams))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(
                        MockMvcResultMatchers.content().json(new ObjectMapper().writeValueAsString(expected)));
    }
}
