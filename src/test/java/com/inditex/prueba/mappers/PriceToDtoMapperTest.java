package com.inditex.prueba.mappers;

import com.inditex.prueba.dto.PriceResponseDto;
import com.inditex.prueba.models.Brand;
import com.inditex.prueba.models.Price;
import com.inditex.prueba.models.Product;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PriceToDtoMapperTest {

    private final PriceToDtoMapper priceToDtoMapper = new PriceToDtoMapper();

    @Test
    public void priceModelToDtoResponseTest() {
        Price dummyPrice = Price.builder()
                .price(2.0)
                .priceList(3L)
                .currency("EUR")
                .brand(new Brand(1L, "ZARA"))
                .product(new Product(55L))
                .startDate(new Timestamp(new Date(97, 10, 18).getTime()))
                .endDate(new Timestamp(new Date(98, 11, 18).getTime())).build();

        PriceResponseDto responseDto = priceToDtoMapper.priceModelToDtoResponse(dummyPrice);

        assertEquals(55L, responseDto.getProductId());
        assertEquals(1L, responseDto.getBrandId());
        assertEquals(3L, responseDto.getPriceList());
        assertEquals(2.0, responseDto.getFinalPrice());
        assertEquals("1997-11-18 00:00:00.0",responseDto.getStartDate());
        assertEquals("1998-12-18 00:00:00.0",responseDto.getEndDate());


    }
}
