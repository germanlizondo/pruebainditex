package com.inditex.prueba.services.impl;

import com.inditex.prueba.dto.PriceResponseDto;
import com.inditex.prueba.exceptions.models.PriceNotFoundException;
import com.inditex.prueba.mappers.PriceToDtoMapper;
import com.inditex.prueba.models.Price;
import com.inditex.prueba.repositories.PriceRepository;
import com.inditex.prueba.services.PriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

@Service
@Slf4j
public class PriceServiceImpl implements PriceService {

    @Autowired
    private PriceRepository priceRepository;
    @Autowired
    private PriceToDtoMapper priceToDtoMapper;

    @Override
    public PriceResponseDto getPrice(final Date dateApplied, final Long productId, final Long brandId) throws PriceNotFoundException {

        Price price =
                priceRepository.getPriceByDate(new Timestamp(dateApplied.getTime()), productId, brandId);

        log.info("Respository return data: [{}]", price);
        return priceToDtoMapper.priceModelToDtoResponse(price);
    }
}
