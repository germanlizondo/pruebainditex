package com.inditex.prueba.services;

import com.inditex.prueba.dto.PriceResponseDto;
import com.inditex.prueba.exceptions.models.PriceNotFoundException;

import java.util.Date;

public interface PriceService {

    PriceResponseDto getPrice(final Date dateApplied, final Long productId, final Long brandId) throws PriceNotFoundException;
}
