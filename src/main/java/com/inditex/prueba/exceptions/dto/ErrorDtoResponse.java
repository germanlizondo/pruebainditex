package com.inditex.prueba.exceptions.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serial;
import java.io.Serializable;

@Data
@AllArgsConstructor
public class ErrorDtoResponse implements Serializable {

    private static final long serialVersionUID = -4642839761570694916L;

    private HttpStatus status;
    private String message;
    private String endpoint;

}
