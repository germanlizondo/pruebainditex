package com.inditex.prueba.exceptions.controllers;

import com.inditex.prueba.exceptions.dto.ErrorDtoResponse;
import com.inditex.prueba.exceptions.models.PriceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class PriceNotFoundExceptionHandle {

    @ResponseBody
    @ExceptionHandler(PriceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDtoResponse handlePriceNotFoundException(final HttpServletRequest request, PriceNotFoundException ex) {
        log.error("Price not found");
        return new ErrorDtoResponse(HttpStatus.BAD_REQUEST, ex.getMessage(), request.getRequestURI());
    }

}
