package com.inditex.prueba.exceptions.models;

import java.sql.Timestamp;

public class PriceNotFoundException extends Exception {

    public PriceNotFoundException() {
        super("Price not found");
    }
}
