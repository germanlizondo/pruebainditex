package com.inditex.prueba.repositories;

import com.inditex.prueba.exceptions.models.PriceNotFoundException;
import com.inditex.prueba.models.Price;

import java.sql.Timestamp;

public interface PriceRepository {

    Price getPriceByDate(final Timestamp dateToApplied, final Long productId, final Long brandId) throws PriceNotFoundException;
}
