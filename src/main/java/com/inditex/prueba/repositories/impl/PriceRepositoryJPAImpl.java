package com.inditex.prueba.repositories.impl;

import com.inditex.prueba.exceptions.models.PriceNotFoundException;
import com.inditex.prueba.models.Price;
import com.inditex.prueba.repositories.abstractions.PriceJPARepository;
import com.inditex.prueba.repositories.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Optional;

@Repository
public class PriceRepositoryJPAImpl implements PriceRepository {

    @Autowired
    private PriceJPARepository priceJPARepository;

    @Override
    public Price getPriceByDate(final Timestamp dateToApplied, final Long productId, final Long brandId) throws PriceNotFoundException {

        Optional<Price> optionalPrice = priceJPARepository.getPriceByDate(dateToApplied, productId, brandId);

        if (optionalPrice.isEmpty()) throw new PriceNotFoundException();

        return optionalPrice.get();
    }
}
