package com.inditex.prueba.mappers;

import com.inditex.prueba.dto.PriceResponseDto;
import com.inditex.prueba.models.Price;
import org.springframework.stereotype.Component;

@Component
public class PriceToDtoMapper {

  public PriceResponseDto priceModelToDtoResponse(final Price price) {
    return new PriceResponseDto(
        price.getProductId(),
        price.getBrandId(),
        price.getPriceList(),
        price.getStartDate().toString(),
        price.getEndDate().toString(),
        price.getPrice());
  }
}
