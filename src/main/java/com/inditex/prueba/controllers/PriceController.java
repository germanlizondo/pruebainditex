package com.inditex.prueba.controllers;

import com.inditex.prueba.dto.PriceResponseDto;
import com.inditex.prueba.exceptions.models.PriceNotFoundException;
import com.inditex.prueba.services.impl.PriceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/price")
public class PriceController {

    @Autowired
    private PriceServiceImpl priceServiceImpl;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PriceResponseDto> getPrice(
            @RequestParam(value = "dateApplied") final Date dateApplied,
            @RequestParam(value = "productId") final Long productId,
            @RequestParam(value = "brandId") final Long brandId) throws PriceNotFoundException {
        log.info(
                "Endpoint /price has been called with query params -> date: [{}], productId: [{}], branchId: [{}]",
                dateApplied,
                productId,
                brandId);

        PriceResponseDto response = priceServiceImpl.getPrice(dateApplied, productId, brandId);

        return ResponseEntity.ok(response);
    }
}
