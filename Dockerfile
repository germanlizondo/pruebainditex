FROM gradle:7.6.1-jdk17-alpine AS builder
WORKDIR /app
COPY . .
RUN gradle clean build

FROM openjdk:17-alpine
WORKDIR /app

COPY --from=builder /app/build/libs/*.jar ./

EXPOSE 8080

CMD ["java", "-jar", "prueba-0.0.1-SNAPSHOT.jar"]
